//Calls another activity, by action and category, without passing data
//refer to AndroidManifest.xml<intent-filter> when determining the action and category of the activity to call
Intent iImp = new Intent("actionName"); //TODO Replace 'actionName' as appropriate for your action (for example, Intent.ACTION_EDIT)
iImp.addCategory("categoryName"); //TODO Replace 'categoryName' as appropriate for your category (for example, Intent.CATEGORY_DEFAULT)
startActivity(iImp);	