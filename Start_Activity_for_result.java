
//Calls another activity, identified by action and category, passing data URL and a MIME type
//The class calling the snippet code must implement the following method:
//protected void onActivityResult (int requestCode, int resultCode, Intent data) {}
Intent iImp = new Intent();
iImp.setAction("actionName"); //TODO Replace 'actionName' as appropriate for your action (for example, Intent.ACTION_EDIT)
iImp.addCategory("categoryName"); //TODO Replace 'categoryName' as appropriate for your category (for example, Intent.CATEGORY_DEFAULT)
//optional - set data and MIME type for the intent
iImp.setDataAndType(Uri.parse("http://com.example.project/folder"), "text/plain"); //TODO Change URL and MIME type as appropriate
startActivityForResult(iImp, 0); //TODO The second parameter (here, zero) is the request code to be used in onActivityResult(); change this parameter to an appropriate value for your activity